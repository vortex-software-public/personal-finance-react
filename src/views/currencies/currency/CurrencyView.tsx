import React, { useEffect, useState } from 'react'
import * as S from './CurrencyView.styles'
import Input from '../../../components/input'
import Button from '../../../components/button'
import { useParams, useHistory } from 'react-router-dom'
import { useDispatch, useSelector } from 'react-redux'
import { fetchCurrency, createCurrency, editCurrency } from '../../../state/currencies/actions'

type props = {

}

const CurrencyView: React.FunctionalComponent<props> = () => {
    const [currencyState, setCurrencyState] = useState({
        id: '',
        name: '',
        abreviature: ''
    })
    const history = useHistory()
    const params = useParams()
    const dispatch = useDispatch()
    const currency = useSelector(state => state.currenciesReducer.currency)
    const loadingCategory = useSelector(state => state.currenciesReducer.loadingCategory)


    useEffect(() => {
        params.id && dispatch(fetchCurrency(params.id))
    }, [])


    // Continuar con logica para reutilizar vista currencyVierw
    useEffect(() => {
        params.id !== 'new' && setCurrencyState(currency)
    }, [currency])

    const handleChangeValue = (value, key) => {
        setCurrencyState({
            ...currencyState,
            [key]: value
        })
    }

    const handleCurrencyCreator = () => {
        if(validateFields()) {
            params.id === 'new'
                ? dispatch(createCurrency(currencyState, history))
                : dispatch(editCurrency(currencyState, history))
        } else {
            alert('Invalid field: verify information')
        }
    }

    const validateFields = () => {
        return ['name', 'abreviature'].every((field) => {
            return currencyState[field] !== ''
        })
    }

    return (
        <S.Container>
            <Input
                inputKey='name'
                label='Name'
                value={currencyState.name}
                onChangeValue={handleChangeValue}/>
            <Input
                inputKey='abreviature'
                label='Abreviature'
                value={currencyState.abreviature}
                onChangeValue={handleChangeValue}/>
            <Button
                loading={loadingCategory}
                text={params.id === 'new' ? 'New' : 'Edit'}
                onClick={handleCurrencyCreator}/>
        </S.Container>
    )
}

export default CurrencyView