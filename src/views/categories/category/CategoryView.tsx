import React, { useEffect, useState } from 'react'
import * as S from './CategoryView.styles'
import Input from '../../../components/input'
import Button from '../../../components/button'
import { useParams, useHistory } from 'react-router-dom'
import { useDispatch, useSelector } from 'react-redux'
import { fetchCategory, createCategory, editCategory } from '../../../state/categories/actions'

type props = {

}

const CategoryView: React.FunctionalComponent<props> = () => {
    const [categoryState, setCategoryState] = useState({
        id: '',
        name: '',
    })
    const history = useHistory()
    const params = useParams()
    const dispatch = useDispatch()
    const category = useSelector(state => state.categoriesReducer.category)
    const loadingCategory = useSelector(state => state.categoriesReducer.loadingCategory)


    useEffect(() => {
        params.id && dispatch(fetchCategory(params.id))
    }, [])


    // Continuar con logica para reutilizar vista categoryVierw
    useEffect(() => {
        params.id !== 'new' && setCategoryState(category)
    }, [category])

    const handleChangeValue = (value, key) => {
        setCategoryState({
            ...categoryState,
            [key]: value
        })
    }

    const handleCategoryCreator = () => {
        if(validateFields()) {
            params.id === 'new'
                ? dispatch(createCategory(categoryState, history))
                : dispatch(editCategory(categoryState, history))
        } else {
            alert('Invalid field: verify information')
        }
    }

    const validateFields = () => {
        return ['name'].every((field) => {
            return categoryState[field] !== ''
        })
    }

    return (
        <S.Container>
            <Input
                inputKey='name'
                label='Name'
                value={categoryState.name}
                onChangeValue={handleChangeValue}/>
            <Button
                loading={loadingCategory}
                text={params.id === 'new' ? 'New' : 'Edit'}
                onClick={handleCategoryCreator}/>
        </S.Container>
    )
}

export default CategoryView
