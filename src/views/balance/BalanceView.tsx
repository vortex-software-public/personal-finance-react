import React, { useEffect } from 'react'
import * as S from './BalanceView.styles'
import { useDispatch, useSelector } from 'react-redux'
import { fetchBalance } from '../../state/balance/actions';
import { LinearProgress } from '@material-ui/core'
import BoxItem from '../../components/boxItem';

type props = {
    
}

const BalanceView: React.FunctionComponent<props> = (props) => {
    const dispatch = useDispatch()
    const balance = useSelector(state => state.balanceReducer.balance)
    const loadingBalance = useSelector(state => state.balanceReducer.loadingBalance)    

    useEffect(() => {
        dispatch(fetchBalance())
    }, [])

    return (
        <S.Container>
            {!loadingBalance 
                ? balance.map((balance) => (
                    <div key={balance.id}>
                        <BoxItem 
                            info={
                                <div>
                                    <span>{`${balance.account_name}`} 
                                        <span style={{fontSize: 13}}>{` (${balance.currency})`}</span>
                                    </span>
                                    <div style={{display: 'flex', flexDirection: 'row', justifyContent: 'space-between'}}>
                                        {/* <div>
                                            <span style={{fontWeight: 'bold', fontSize: 12}}>
                                                {`${balance.label_next_month} ${balance.label_next_month_amount}`} 
                                            </span>
                                            <span style={{fontSize: 12}}>{` (${balance.currency})`}</span>
                                        </div> */}
                                        {/* <span style={{fontSize: 11}}>{`${balance.label_next_month_percent} `}
                                            {balance.label_next_month_percent.includes('-')
                                                ? <span style={{color: 'red'}}>↓</span>
                                                : <span style={{color: 'green'}}>↑</span>
                                            }
                                        </span> */}
                                    </div>
                                </div>
                            }
                            description={
                                <div style={{display: 'flex', flexDirection: 'column', alignItems: 'flex-end'}}>
                                    <div style={{fontSize: 13, fontWeight: 'bold'}}>
                                        <span>{`Balance: `}</span>
                                        <span>{`${balance.label_balance} `}</span> 
                                        <span>{`(${balance.currency})`}</span>
                                    </div>
                                    <div style={{fontSize: 12}}>
                                        <span>{`Total ingresos: `}</span>
                                        <span>{`${balance.label_total_ingresos} `}</span>
                                        <span>{`(${balance.currency})`}</span>
                                    </div>
                                    <div style={{fontSize: 12}}>
                                        <span>{`Total egresos: `}</span>
                                        <span>{`${balance.label_total_egresos} `}</span>
                                        <span>{`(${balance.currency})`}</span>
                                    </div>
                                </div>
                            }
                        />
                    </div>))
                : <LinearProgress color="secondary" />}
        </S.Container>
    )
}

export default BalanceView