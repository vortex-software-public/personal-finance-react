import types from './types'

const initialState = {
    currencies: [],
    loadingCurrencies: false,
    currency: {},
    loadingCurrency: false
}

const currenciesReducer = (state = initialState, action) => {
    switch (action.type) {
        case types.SET_CURRENCIES:
            return {
                ...state,
                currencies: action.currencies
            }
        
        case types.LOADING_CURRENCIES:
            return {
                ...state,
                loadingCurrencies: action.loadingCurrencies
            }
            
        case types.SET_CURRENCY:
            return {
                ...state,
                currency: action.currency
            }

        case types.LOADING_CURRENCY: 
            return {
                ...state,
                loadingCurrency: action.loadingCurrency
            }
            
        default:
            return state;
    }
}

export default currenciesReducer