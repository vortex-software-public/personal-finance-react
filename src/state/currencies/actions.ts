import types from './types'
import services from '../../services/index'

const fetchCurrencies = () => {
    return (dispatch) => {
        dispatch(loadingCurrencies(true))
        services.fetchCurrencies(
            (response) => {
                dispatch(loadingCurrencies(false))
                dispatch(setCurrencies(response.data.currencies))
            },
            (error) => {
                alert(error)
            }
        )
    }
}

const setCurrencies = (currencies) => {
    return {
        type: types.SET_CURRENCIES,
        currencies
    }
}

const loadingCurrencies = (loadingCurrencies) => {
    return {
        type: types.LOADING_CURRENCIES,
        loadingCurrencies
    }
}

const fetchCurrency = (id) => {
    return (dispatch) => {
        dispatch(loadingCurrency(true))
        services.fetchCurrency(
            id,
            (response) => {
                dispatch(loadingCurrency(false))
                const { data: { currency } } = response
                dispatch(setCurrency(currency))
            },
            (error) => {
                console.log(error)
            }
        )
    }
}

const loadingCurrency = (loadingCurrency) => {
    return {
        type: types.LOADING_CURRENCY,
        loadingCurrency
    }
}

const setCurrency = (currency) => {
    return {
        type: types.SET_CURRENCY,
        currency
    }
}

const createCurrency = (currency, history) => {
    return (dispatch) => {
        dispatch(loadingCurrency(true))
        services.createCurrency(
            currency,
            (response) => {
                const { data: { message } } = response
                alert(message)
                dispatch(loadingCurrency(false))
                history.push('/currencies')
            },
            (error) => {
                console.log(error)
            }
        )
    }
}

const editCurrency = (currency, history) => {
    return (dispatch) => {
        dispatch(loadingCurrency(true))
        services.editCurrency(
            currency,
            (response) => {
                const { data: { message } } = response
                alert(message)
                dispatch(loadingCurrency(false))
                history.push('/currencies')
            },
            (error) => {
                console.log(error)
            }
        )
    }
}

const deleteCurrency = (id) => {
    return (dispatch) => {
        dispatch(loadingCurrencies(true))
        services.deleteCurrency(
            id,
            (response) => {
                const { data: { message } } = response
                alert(message)
                dispatch(fetchCurrencies())
            },
            (error) => {
                console.log(error)
            }
        )
    }
}

export {
    fetchCurrencies,
    fetchCurrency,
    createCurrency,
    editCurrency,
    deleteCurrency
}