import types from './types'
import services from '../../services/index'

const loggingIn = (loggingIn) => {
    return {
        type: types.LOGGING_IN,
        loggingIn
    }
}

const login = (email, password, history) => {
    return (dispatch) => {
        dispatch(loggingIn(true))
        const data = {email, password}
        services.login(data, 
            (response) => {
                dispatch(loggingIn(false))
                dispatch(setLogged(true))
                history.push('/')
            },
            (error) => {
                alert(error)
            })
    }
}

const setAdmin = (isAdmin) => {
    return {
        type: types.SET_ADMIN,
        isAdmin
    }
}

const setLogged = (isLogged) => {
    return {
        type: types.SET_LOGGED,
        isLogged
    }
}

export {
    login,
    setAdmin,
    setLogged
}