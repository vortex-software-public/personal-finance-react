import services from '../../services/index'
import types from './types'

const fetchCategories = () => {
    return (dispatch) => {
        dispatch(loadingCategories(true))
        services.fetchCategories(
            (response) => {
                dispatch(loadingCategories(false))
                const { data: { categories } } = response
                dispatch(setCategories(categories))
            },
            (error) => {
                console.log(error)
            }
        )
    }
}

const loadingCategories = (loadingCategories) => {
    return {
        type: types.LOADING_CATEGORIES,
        loadingCategories
    }
}

const setCategories = (categories) => {
    return {
        type: types.SET_CATEGORIES,
        categories
    }
}

const fetchCategory = (id) => {
    return (dispatch) => {
        dispatch(loadingCategory(true))
        services.fetchCategory(
            id,
            (response) => {
                dispatch(loadingCategory(false))
                const { data: { category } } = response
                dispatch(setCategory(category))
            },
            (error) => {
                console.log(error)
            }
        )
    }
}

const loadingCategory = (loadingCategory) => {
    return {
        type: types.LOADING_CATEGORY,
        loadingCategory
    }
}

const setCategory = (category) => {
    return {
        type: types.SET_CATEGORY,
        category
    }
}

const createCategory = (category, history) => {
    return (dispatch) => {
        dispatch(loadingCategory(true))
        services.createCategory(
            category,
            (response) => {
                const { data: { message } } = response
                alert(message)
                dispatch(loadingCategory(false))
                history.push('/categories')
            },
            (error) => {
                console.log(error)
            }
        )
    }
}

const editCategory = (category, history) => {
    return (dispatch) => {
        dispatch(loadingCategory(true))
        services.editCategory(
            category,
            (response) => {
                const { data: { message } } = response
                alert(message)
                dispatch(loadingCategory(false))
                history.push('/categories')
            },
            (error) => {
                console.log(error)
            }
        )
    }
}

const deleteCategory = (id) => {
    return (dispatch) => {
        dispatch(loadingCategories(true))
        services.deleteCategory(
            id,
            (response) => {
                const { data: { message } } = response
                alert(message)
                dispatch(fetchCategories())
            },
            (error) => {
                console.log(error)
            }
        )
    }
}

export {
    fetchCategories,
    fetchCategory,
    createCategory,
    editCategory,
    deleteCategory
}