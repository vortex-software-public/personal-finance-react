import types from './types';

const initialState = {
    categories: [],
    loadingCategories: false,
    category: {},
    loadingCategory: false
}

const categoriesReducer = (state = initialState, action) => {
    switch (action.type) {
        case types.SET_CATEGORIES:
            return {
                ...state,
                categories: action.categories
            }

        case types.LOADING_CATEGORIES: 
            return {
                ...state,
                loadingCategories: action.loadingCategories
            }
            
        case types.SET_CATEGORY:
            return {
                ...state,
                category: action.category
            }

        case types.LOADING_CATEGORY:
            return {
                ...state,
                loadingCategory: action.loadingCategory
            }

        default:
            return state
    }
}

export default categoriesReducer