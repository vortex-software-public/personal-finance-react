import services from '../../services/index'
import types from './types'


const fetchBalance = () => {
    return (dispatch) => {
        dispatch(loadingBalance(true))
        services.fetchBalance(
            (response) => {
                dispatch(loadingBalance(false))
                const { data: { accounts } } = response
                dispatch(setBalance(accounts))
            },
            (error) => {
                console.log(error)
            }
        )
    }
}

const loadingBalance = (loadingBalance) => {
    return {
        type: types.LOADING_BALANCE,
        loadingBalance
    }
}

const setBalance = (balance) => {
    return {
        type: types.SET_BALANCE,
        balance
    }
}

export {
    fetchBalance
}