import types from './types';

const initialState = {
    balance: [],
    loadingBalance: false,
}

const balanceReducer = (state = initialState, action) => {
    switch (action.type) {
        case types.SET_BALANCE:
            return {
                ...state,
                balance: action.balance
            }

        case types.LOADING_BALANCE: 
            return {
                ...state,
                loadingBalance: action.loadingBalance
            }
            
        default:
            return state
    }
}

export default balanceReducer