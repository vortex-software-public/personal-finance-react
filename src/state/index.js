import { combineReducers } from 'redux'
import currenciesReducer from './currencies/reducer'
import clientsReducer from './clients/reducer'
import appReducer from './app/reducer'
import categoriesReducer from './categories/reducer'
import reportsReducer from './reports/reducer'
import transactionsReducer from './transactions/reducer'
import balanceReducer from './balance/reducer'

const rootReducer = combineReducers({
    currenciesReducer,
    clientsReducer,
    appReducer,
    categoriesReducer,
    reportsReducer,
    transactionsReducer,
    balanceReducer
})

export default rootReducer