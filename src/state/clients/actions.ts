import services from '../../services/index'
import types from './types'

const fetchClients = () => {
    return (dispatch) => {
        dispatch(loadingClients(true))
        services.fetchClients(
            (response) => {
                dispatch(loadingClients(false))
                const { data: { clients } } = response
                dispatch(setClients(clients))
            },
            (error) => {
                console.log(error)
            }
        )
    }
}

const loadingClients = (loadingClients) => {
    return {
        type: types.LOADING_CLIENTS,
        loadingClients
    }
}

const setClients = (clients) => {
    return {
        type: types.SET_CLIENTS,
        clients
    }
}

const fetchClient = (id) => {
    return (dispatch) => {
        dispatch(loadingClient(true))
        services.fetchClient(
            id,
            (response) => {
                dispatch(loadingClient(false))
                const { data: { client } } = response
                dispatch(setClient(client))
            },
            (error) => {
                console.log(error)
            }
        )
    }
}

const loadingClient = (loadingClient) => {
    return {
        type: types.LOADING_CLIENT,
        loadingClient
    }
}

const setClient = (client) => {
    return {
        type: types.SET_CLIENT,
        client
    }
}

const createClient = (client, history) => {
    return (dispatch) => {
        dispatch(loadingClient(true))
        services.createClient(
            client,
            (response) => {
                const { data: { message } } = response
                alert(message)
                dispatch(loadingClient(false))
                history.push('/clients')
            },
            (error) => {
                console.log(error)
            }
        )
    }
}

const editClient = (client, history, route) => {
    return (dispatch) => {
        dispatch(loadingClient(true))
        services.editClient(
            client,
            (response) => {
                const { data: { message } } = response
                alert(message)
                dispatch(loadingClient(false))
                history.push(route)
            },
            (error) => {
                console.log(error)
            }
        )
    }
}

const deleteClient = (id) => {
    return (dispatch) => {
        dispatch(loadingClients(true))
        services.deleteClient(
            id,
            (response) => {
                const { data: { message } } = response
                alert(message)
                dispatch(fetchClients())
            },
            (error) => {
                console.log(error)
            }
        )
    }
}

export {
    fetchClients,
    fetchClient,
    createClient,
    editClient,
    deleteClient
}