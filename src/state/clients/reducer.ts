import types from './types';

const initialState = {
    clients: [],
    loadingClients: false,
    client: {},
    loadingClient: false
}

const clientsReducer = (state = initialState, action) => {
    switch (action.type) {
        case types.SET_CLIENTS:
            return {
                ...state,
                clients: action.clients
            }

        case types.LOADING_CLIENTS: 
            return {
                ...state,
                loadingClients: action.loadingClients
            }
            
        case types.SET_CLIENT:
            return {
                ...state,
                client: action.client
            }

        case types.LOADING_CLIENT:
            return {
                ...state,
                loadingClient: action.loadingClient
            }

        default:
            return state
    }
}

export default clientsReducer