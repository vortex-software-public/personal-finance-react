import services from "../../services"
import types from "./types"

const loadingReport = (loadingReport) => {
    return {
        type: types.LOADING_REPORT,
        loadingReport
    }
}

const setExpensesForecast = (expensesForecast) => {
    return {
        type: types.SET_EXPENSES_FORECAST,
        expensesForecast
    }
}

const setTransactionsFiltered = (transactionsFiltered) => {
    return {
        type: types.SET_TRANSACTIONS_FILTERED,
        transactionsFiltered
    }
}

const fetchExpensesForecast = () => {
    return (dispatch) => {
        dispatch(loadingReport(true))
        services.fetchExpensesForecast(
            {},
            (response) => {
                dispatch(loadingReport(false))
                const { data: { accounts } } = response
                dispatch(setExpensesForecast(accounts))
            },
            () => {}
        )
    }
}

const fetchExpensesByMonthsByDay = () => {
    debugger
    return (dispatch) => {
        dispatch(loadingReport(true))
        services.fetchExpensesByMonthsByDay(
            {},
            (response) => {
                debugger
                dispatch(loadingReport(false))
                const { data: { transactions_filtered } } = response
                dispatch(setTransactionsFiltered(transactions_filtered))
            },
            () => {}
        )
    }
}

const fetchMovements = () => {
    return (dispatch) => {
        dispatch(loadingReport(true))
        services.fetchMovements(
            {},
            (response) => {
                debugger
                dispatch(loadingReport(false))
                const { data: { transactions_filtered } } = response
                dispatch(setTransactionsFiltered(transactions_filtered))
            },
            () => {}
        )
    }
}

const fetchExpensesByMonthsByCategory = () => {
    return (dispatch) => {
        dispatch(loadingReport(true))
        services.fetchExpensesByMonthsByCategory(
            {},
            (response) => {
                dispatch(loadingReport(false))
                const { data: { transactions_filtered } } = response
                dispatch(setTransactionsFiltered(transactions_filtered))
            },
            () => {}
        )
    }
}

export {
    fetchExpensesForecast,
    fetchExpensesByMonthsByDay,
    fetchMovements,
    fetchExpensesByMonthsByCategory
}