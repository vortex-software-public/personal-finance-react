import services from '../../services/index'
import types from './types'

const fetchTransactions = () => {
    return (dispatch) => {
        dispatch(loadingTransactions(true))
        services.fetchTransactions(
            (response) => {
                dispatch(loadingTransactions(false))
                const { data: { transactions } } = response
                dispatch(setTransactions(transactions))
            },
            (error) => {
                console.log(error)
            }
        )
    }
}

const loadingTransactions = (loadingTransactions) => {
    return {
        type: types.LOADING_TRANSACTIONS,
        loadingTransactions
    }
}

const setTransactions = (transactions) => {
    return {
        type: types.SET_TRANSACTIONS,
        transactions
    }
}

const fetchTransaction = (id) => {
    return (dispatch) => {
        dispatch(loadingTransaction(true))
        services.fetchTransaction(
            id,
            (response) => {
                dispatch(loadingTransaction(false))
                const { data: { transaction } } = response
                dispatch(setTransaction(transaction))
            },
            (error) => {
                console.log(error)
            }
        )
    }
}

const loadingTransaction = (loadingTransaction) => {
    return {
        type: types.LOADING_TRANSACTION,
        loadingTransaction
    }
}

const setTransaction = (transaction) => {
    return {
        type: types.SET_TRANSACTION,
        transaction
    }
}

const createTransaction = (transaction, history) => {
    return (dispatch) => {
        dispatch(loadingTransaction(true))
        services.createTransaction(
            transaction,
            (response) => {
                const { data: { message } } = response
                alert(message)
                dispatch(loadingTransaction(false))
                history.push('/transactions')
            },
            (error) => {
                console.log(error)
            }
        )
    }
}

const editTransaction = (transaction, history) => {
    return (dispatch) => {
        dispatch(loadingTransaction(true))
        services.editTransaction(
            transaction,
            (response) => {
                const { data: { message } } = response
                alert(message)
                dispatch(loadingTransaction(false))
                history.push('/transactions')
            },
            (error) => {
                console.log(error)
            }
        )
    }
}

const deleteTransaction = (id) => {
    return (dispatch) => {
        dispatch(loadingTransactions(true))
        services.deleteTransaction(
            id,
            (response) => {
                const { data: { message } } = response
                alert(message)
                dispatch(fetchTransactions())
            },
            (error) => {
                console.log(error)
            }
        )
    }
}

export {
    fetchTransactions,
    fetchTransaction,
    createTransaction,
    editTransaction,
    deleteTransaction
}