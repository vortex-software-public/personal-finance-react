import types from './types';

const initialState = {
    transactions: [],
    loadingTransactions: false,
    transaction: {},
    loadingTransaction: false
}

const transactionReducer = (state = initialState, action) => {
    switch (action.type) {
        case types.SET_TRANSACTIONS:
            return {
                ...state,
                transactions: action.transactions
            }

        case types.LOADING_TRANSACTIONS: 
            return {
                ...state,
                loadingTransactions: action.loadingTransactions
            }
            
        case types.SET_TRANSACTION:
            return {
                ...state,
                transaction: action.transaction
            }

        case types.LOADING_TRANSACTION:
            return {
                ...state,
                loadingTransaction: action.loadingTransaction
            }

        default:
            return state
    }
}

export default transactionReducer