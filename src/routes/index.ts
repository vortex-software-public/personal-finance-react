import LoginView from "../views/login/LoginView";
import CurrenciesView from "../views/currencies/CurrenciesView";
import ClientView from "../views/clients/client/ClientView";
import ClientsView from "../views/clients/ClientsView";
import CategoriesView from "../views/categories/CategoriesView";
import CategoryView from "../views/categories/category/CategoryView";
import ReportsView from "../views/reports/ReportsView";
import TransactionsView from "../views/transactions/TransactionsView";
import BalanceView from "../views/balance/BalanceView";
import CurrencyView from "../views/currencies/currency/CurrencyView";
import TransactionView from "../views/transactions/trasaction/TransactionView";

const routes = [
    {
        path: '/login',
        component: LoginView,
        exact: true
    },
    {
        path: '/clients',
        component: ClientsView,
        exact: true
    },
    {
        path: '/clients/:id',
        component: ClientView,
        exact: true
    },
    {
        path: '/clients/new',
        component: ClientView,
        exact: true
    },
    {
        path: '/categories',
        component: CategoriesView,
        exact: true
    },
    {
        path: '/categories/:id',
        component: CategoryView,
        exact: true
    },
    {
        path: '/categories/new',
        component: CategoryView,
        exact: true
    },
    {
        path: '/currencies',
        component: CurrenciesView,
        exact: true
    },
    {
        path: '/currencies/:id',
        component: CurrencyView,
        exact: true
    },
    {
        path: '/reports',
        component: ReportsView,
        exact: true
    },
    {
        path: '/transactions',
        component: TransactionsView,
        exact: true
    },
    {
        path: '/transactions/:id',
        component: TransactionView,
        exact: true
    },
    {
        path: '/transactions/new',
        component: TransactionView,
        exact: true
    },
    {
        path: '/profile/:id',
        component: ClientView,
        exact: true
    },
    {
        path: '/balance',
        component: BalanceView,
        data: { title: "Heroes List" },
        exact: true
    }
]

export default routes
